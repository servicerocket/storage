Storage
=======

2015-11-27, 5.5.0
-----------------
 - WIKI-580 Confluence 5.9.1 compatibility.

 2015-10-05, 5.4.0
 -----------------
  - WIKI-483 Confluence 5.9 compatibility.

2015-04-21, 5.3.1
-----------------
 - AN-22-1249 Fix OSGi compatibility with newer versions of xstream.

2015-04-06, 5.3.0
-----------------
 - WIKI-204 Implement retrieve storage path as an array.

2014-10-21, 5.2.4
-----------------
 - WIKI-73 Fix OSGi directive issue from previous release.