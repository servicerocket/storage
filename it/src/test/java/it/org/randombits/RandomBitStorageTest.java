package it.org.randombits;

import com.atlassian.confluence.it.User;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import it.com.servicerocket.randombits.AddOnTest;
import it.com.servicerocket.randombits.pageobject.AddOnOSGIPage;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author HengHwa
 * @since 1.0.7.20141009
 */
public class RandomBitStorageTest extends AbstractWebDriverTest {
    @Test public void testRandomBitsPluginIsInstalled() throws Exception {
        String addonName = "RB Storage - Core";

        boolean addonIsInstalled = new AddOnTest().isInstalled(
            product.login(User.ADMIN, AddOnOSGIPage.class),
            addonName
        );

        assertThat(
            addonName + " is installed",
            addonIsInstalled
        );
    }
}
