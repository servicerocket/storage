/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * 			 notice, this list of conditions and the following disclaimer in the
 *   		 documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 * 			 may be used to endorse or promote products derived from this software
 * 			 without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.storage.confluence;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import org.randombits.storage.ObjectBasedStorage;

import com.atlassian.renderer.RenderContext;

/**
 * Provides a storage implementation for accessing properties stored in the
 * RenderContext.
 * 
 * @author david@randombits.org
 */
public class RenderContextStorage extends ObjectBasedStorage {
    private RenderContext renderContext;

    public RenderContextStorage( RenderContext renderContext ) {
        super( BoxType.Real );
        this.renderContext = renderContext;
    }

    @Override
    protected Set<String> baseNameSet() {
        Map<Object, Object> params = renderContext.getParams();
        if ( params != null ) {
            Set<String> names = new java.util.HashSet<String>();
            for ( Object o : params.keySet() ) {
                if ( o instanceof String )
                    names.add( ( String ) o );
            }
            return names;
        }
        return Collections.EMPTY_SET;
    }

    @Override
    protected Object getBaseObject( String name ) {
        return renderContext.getParam( name );
    }

    @Override
    protected void setBaseObject( String name, Object value ) {
        renderContext.addParam( name, value );
    }

    /**
     * @see org.randombits.storage.Storage#isReadOnly()
     */
    @Override
    public boolean isReadOnly() {
        return false;
    }
}
