[ ![Codeship Status for servicerocket/storage](https://codeship.io/projects/7dd30bf0-3b6e-0132-01f2-7235cdea93e5/status)](https://codeship.io/projects/42652)

Storage
=======

This library is an Atlassian plugin that provides a common API for accessing 
stored data in number of formats, with different back-ends.

