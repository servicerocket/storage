/*
 * Copyright (c) 2005, David Peterson 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * 			 notice, this list of conditions and the following disclaimer in the
 *   		 documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 * 			 may be used to endorse or promote products derived from this software
 * 			 without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * v1.0: Created on Sep 20, 2005 by David Peterson
 */
package org.randombits.storage.servlet;

import org.randombits.storage.ObjectBasedStorage;

import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.Set;

/**
 * 
 * @author David Peterson
 */
public class SessionStorage extends ObjectBasedStorage {
    HttpSession session;

    /**
     * @param session
     *            The HTTP session the data is stored in.
     */
    public SessionStorage( HttpSession session ) {
        super( BoxType.Real );
        this.session = session;
    }

    @Override protected Set<String> baseNameSet() {
        Enumeration<String> e = session.getAttributeNames();
        Set<String> names = new java.util.HashSet<String>();
        while ( e.hasMoreElements() )
            names.add( e.nextElement() );
        return names;
    }

    @Override protected Object getBaseObject( String name ) {
        return session.getAttribute( name );
    }

    /**
     * The HTTP session that this storage object is attached to.
     * 
     * @return The session object.
     */
    public HttpSession getSession() {
        return session;
    }

    @Override public boolean isReadOnly() {
        return false;
    }

    @Override protected void setBaseObject( String name, Object value ) {
        session.setAttribute( name, value );
    }
}
