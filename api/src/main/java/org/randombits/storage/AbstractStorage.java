/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.storage;

import java.util.*;

import org.joda.time.DateTime;

/**
 * A box is a hierarchical storage mechanism. BoxType are intended to preserve
 * values between page loads. They support storing most value types, including
 * primatives (e.g. int, boolean, etc) and objects. Objects do not necessarily
 * have to be serializable to be stored, either.
 * 
 * BoxType may also be read-only, in which case an
 * {@link java.lang.UnsupportedOperationException} will be thrown when a 'set'
 * method is called. To test if a box is read-only, call the {@link #isReadOnly}
 * method.
 * 
 * @author David Peterson
 */
public abstract class AbstractStorage implements Storage {
    private List<StorageListener> listeners;

    private Stack<String> boxNames;

    /**
     * @see org.randombits.storage.Storage#closeBox()
     */
    public void closeBox() {
        if ( boxNames == null || boxNames.size() == 0 )
            throw new StorageException( "There are no more boxes to close." );

        String name = ( String ) boxNames.pop();
        fireBoxClosedEvent( name );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#findBoolean(java.lang.String,
     *      boolean)
     */
    public boolean getBoolean( String name, boolean def ) {
        return getBoolean( name, Boolean.valueOf( def ) ).booleanValue();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#findBoolean(java.lang.String,
     *      java.lang.Boolean)
     */
    public abstract Boolean getBoolean( String name, Boolean def );

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#getDate(java.lang.String,
     *      java.util.Date)
     */
    public abstract Date getDate( String name, Date def );

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#getDateTime(java.lang.String,
     *      org.joda.time.DateTime)
     */
    public DateTime getDateTime( String name, DateTime def ) {
        Date value = getDate( name, null );
        if ( value == null )
            return def;
        return new DateTime( value );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#getDouble(java.lang.String, double)
     */
    public double getDouble( String name, double def ) {
        return getDouble( name, new Double( def ) ).doubleValue();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#getDouble(java.lang.String,
     *      java.lang.Double)
     */
    public abstract Double getDouble( String name, Double def );

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#getInteger(java.lang.String, int)
     */
    public int getInteger( String name, int def ) {
        return getInteger( name, new Integer( def ) ).intValue();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#getInteger(java.lang.String,
     *      java.lang.Integer)
     */
    public abstract Integer getInteger( String name, Integer def );

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#getLong(java.lang.String, long)
     */
    public long getLong( String name, long def ) {
        return getLong( name, new Long( def ) ).longValue();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#getLong(java.lang.String,
     *      java.lang.Long)
     */
    public abstract Long getLong( String name, Long def );

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#getObject(java.lang.String,
     *      java.lang.Object)
     */
    public Object getObject( String name, Object def ) {
        return getObject( name, def, Object.class );
    }

    /**
     * @see Storage#getObject(String, Object, Class)
     */
    public abstract <T> T getObject( String name, T def, Class<T> clazz );

    /**
     * @see org.randombits.storage.Storage#getObjectList(java.lang.String,
     *      java.util.List)
     */
    public abstract List<?> getObjectList( String name, List<?> def );

    /**
     * @see org.randombits.storage.Storage#getString(java.lang.String,
     *      java.lang.String)
     */
    public abstract String getString( String name, String def );

    /**
     * @see org.randombits.storage.Storage#getStringArray(java.lang.String,
     *      java.lang.String[])
     */
    public abstract String[] getStringArray( String name, String[] def );

    /**
     * @see org.randombits.storage.Storage#getStringSet(java.lang.String,
     *      java.util.Set)
     */
    public Set<String> getStringSet( String name, Set<String> def ) {
        String[] valueArray = getStringArray( name, null );

        if ( valueArray == null || valueArray.length == 0 )
            return def;

        Set<String> value = new java.util.HashSet<String>();
        value.addAll( Arrays.asList( valueArray ) );
        return value;
    }

    /**
     * @see org.randombits.storage.Storage#isReadOnly()
     */
    public abstract boolean isReadOnly();

    /**
     * @see org.randombits.storage.Storage#makePath()
     */
    public String makePath() {
        return buildPath().toString();
    }

    /**
     * @see org.randombits.storage.Storage#makePath(java.lang.String)
     */
    public String makePath( String name ) {
        StringBuffer out = buildPath();

        if ( out.length() > 0 )
            out.append( SEPARATOR );
        out.append( name );

        return out.toString();
    }

    private StringBuffer buildPath() {
        StringBuffer out = new StringBuffer();

        if ( boxNames != null ) {
            for ( String name : boxNames ) {
                if ( out.length() > 0 )
                    out.append( SEPARATOR );
                out.append( name );
            }
        }
        return out;
    }

    @Override
    public List<String> makePathAsArray() {
        List<String> path = new ArrayList<>();

        if (boxNames != null) {
            for (String name : boxNames) {
                path.add(name);
            }
        }

        return path;
    }

    @Override
    public List<String> makePathAsArray(String name) {
        List<String> path = makePathAsArray();
        path.add(name);

        return path;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#nameSet()
     */
    public abstract Set<String> nameSet();

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#openBox(java.lang.String)
     */
    public void openBox( String name ) {
        if ( boxNames == null )
            boxNames = new java.util.Stack<String>();

        boxNames.push( name );

        fireBoxOpenedEvent( name );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#setBoolean(java.lang.String, boolean)
     */
    public void setBoolean( String name, boolean value ) {
        setBoolean( name, Boolean.valueOf( value ) );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#setBoolean(java.lang.String,
     *      java.lang.Boolean)
     */
    public abstract void setBoolean( String name, Boolean value );

    /**
     * @see org.randombits.storage.Storage#setDate(java.lang.String,
     *      java.util.Date)
     */
    public abstract void setDate( String name, Date value );

    /**
     * @see org.randombits.storage.Storage#setDateTime(java.lang.String,
     *      org.joda.time.DateTime)
     */
    public void setDateTime( String name, DateTime value ) {
        setDate( name, value == null ? null : value.toDate() );
    }

    /**
     * @see org.randombits.storage.Storage#setDouble(java.lang.String, double)
     */
    public void setDouble( String name, double value ) {
        setDouble( name, new Double( value ) );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#setDouble(java.lang.String,
     *      java.lang.Double)
     */
    public abstract void setDouble( String name, Double value );

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#setInteger(java.lang.String, int)
     */
    public void setInteger( String name, int value ) {
        setInteger( name, new Integer( value ) );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#setInteger(java.lang.String,
     *      java.lang.Integer)
     */
    public abstract void setInteger( String name, Integer value );

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#setLong(java.lang.String, long)
     */
    public void setLong( String name, long value ) {
        setLong( name, new Long( value ) );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#setLong(java.lang.String,
     *      java.lang.Long)
     */
    public abstract void setLong( String name, Long value );

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#setObject(java.lang.String,
     *      java.lang.Object)
     */
    public abstract void setObject( String name, Object value );

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#setObjectList(java.lang.String,
     *      java.util.List)
     */
    public abstract void setObjectList( String name, List<?> value );

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#setString(java.lang.String,
     *      java.lang.String)
     */
    public abstract void setString( String name, String value );

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#setStringArray(java.lang.String,
     *      java.lang.String[])
     */
    public abstract void setStringArray( String name, String[] value );

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#setStringSet(java.lang.String,
     *      java.util.Set)
     */
    public void setStringSet( String name, Set<String> values ) {
        checkReadOnly();

        String[] valuesArray = null;
        if ( values != null ) {
            valuesArray = new String[values.size()];
            valuesArray = values.toArray( valuesArray );
        }
        setStringArray( name, valuesArray );
    }

    /**
     * @throws UnsupportedOperationException
     *             if the storage object is read-only.
     */
    protected void checkReadOnly() throws UnsupportedOperationException {
        if ( isReadOnly() )
            throw new UnsupportedOperationException( "This storage object is read-only" );
    }

    public void addStorageListener( StorageListener listener ) {
        if ( listeners == null ) {
            listeners = new java.util.ArrayList<StorageListener>();
        }
        listeners.add( listener );
    }

    public void removeStorageListener( StorageListener listener ) {
        if ( listeners != null ) {
            listeners.remove( listener );
        }
    }

    protected void fireBoxOpenedEvent( String boxName ) {
        if ( listeners != null ) {
            StorageEvent evt = new StorageEvent( this, boxName );
            for ( StorageListener l : listeners )
                l.boxOpened( evt );
        }
    }

    protected void fireBoxClosedEvent( String boxName ) {
        if ( listeners != null ) {
            StorageEvent evt = new StorageEvent( this, boxName );
            for ( StorageListener l : listeners )
                l.boxClosed( evt );
        }
    }
}
