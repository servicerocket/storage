/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.storage;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;

/**
 * Provides utility methods for working with Storage instances.
 */
public final class StorageUtils {

    private static Map<Storage, Synchronizer> synchros;

    private StorageUtils() {
    }

    private static class Synchronizer implements StorageListener {
        private Set<Storage> targets;

        public Synchronizer() {
            targets = new java.util.HashSet<Storage>();
        }

        public void addTarget( Storage target ) {
            targets.add( target );
        }

        public void removeTarget( Storage target ) {
            targets.remove( target );
        }

        public void boxOpened( StorageEvent evt ) {
            for ( Storage storage : targets ) {
                storage.openBox( evt.getKey() );
            }
        }

        public void boxClosed( StorageEvent evt ) {
            for ( Storage storage : targets ) {
                storage.closeBox();
            }
        }

        public int getTargetCount() {
            return targets.size();
        }
    }

    /**
     * Returns a read-only Storage object which accesses the specified Storage
     * instance.
     * 
     * @param storage
     *            The storage object to make read-only.
     * @return The read-only Storage instance.
     */
    public static Storage readOnlyStorage( Storage storage ) {
        return new ChainedStorage( new Storage[]{storage} ) {
            @Override public boolean isReadOnly() {
                return true;
            }
        };
    }

    /**
     * Synchronises the two storage instances. When the <code>source</code>
     * has a box opened or closed, the same box will be opened or closed in the
     * <code>target</code>. This is a one-way process - the reverse does
     * <code>not</code> happen. That is, if the <code>target</code> has a
     * box opened or closed, it is not reflected in the <code>source</code>.
     * 
     * @param source
     *            The source storage.
     * @param target
     *            The target storage.
     */
    public static void synchronize( Storage source, Storage target ) {
        Synchronizer synchro = null;
        if ( synchros == null ) {
            synchros = new java.util.WeakHashMap<Storage, Synchronizer>();
        } else {
            synchro = synchros.get( source );
        }

        if ( synchro == null ) {
            synchro = new Synchronizer();
            synchros.put( source, synchro );
            source.addStorageListener( synchro );
        }

        synchro.addTarget( target );
    }

    /**
     * Turns off any synchronization established with the
     * {@link #synchronize(Storage,Storage)} method.
     * 
     * @param source
     *            The source storage.
     * @param target
     *            The target storage.
     */
    public static void desynchronize( Storage source, Storage target ) {
        if ( synchros != null ) {
            Synchronizer synchro = synchros.get( source );
            if ( synchro != null ) {
                synchro.removeTarget( target );
                if ( synchro.getTargetCount() == 0 ) {
                    synchros.remove( source );
                    source.removeStorageListener( synchro );
                }
            }
        }
    }

    public static Boolean findBoolean( Storage storage, String path, Boolean defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            Boolean value = storage.getBoolean( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setBoolean( Storage storage, String path, Boolean value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setBoolean( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static boolean findBoolean( Storage storage, String path, boolean defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            boolean value = storage.getBoolean( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setBoolean( Storage storage, String path, boolean value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setBoolean( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static Date findDate( Storage storage, String path, Date defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            Date value = storage.getDate( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setDate( Storage storage, String path, Date value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setDate( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static DateTime findDateTime( Storage storage, String path, DateTime defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            DateTime value = storage.getDateTime( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setDateTime( Storage storage, String path, DateTime value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setDateTime( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static Double findDouble( Storage storage, String path, Double defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            Double value = storage.getDouble( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setDouble( Storage storage, String path, double value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setDouble( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static double findDouble( Storage storage, String path, double defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            double value = storage.getDouble( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setDouble( Storage storage, String path, Double value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setDouble( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static Integer findInteger( Storage storage, String path, Integer defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            Integer value = storage.getInteger( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setInteger( Storage storage, String path, Integer value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setInteger( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static int findInteger( Storage storage, String path, int defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            int value = storage.getInteger( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setInteger( Storage storage, String path, int value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setInteger( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static Long findLong( Storage storage, String path, Long defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            Long value = storage.getLong( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setLong( Storage storage, String path, Long value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setLong( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static long findLong( Storage storage, String path, long defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            long value = storage.getLong( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setLong( Storage storage, String path, long value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setLong( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static Number findNumber( Storage storage, String path, Number defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            Number value = storage.getNumber( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static Object findObject( Storage storage, String path, Object defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            Object value = storage.getObject( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setObject( Storage storage, String path, Object value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setObject( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static List<?> findObjectList( Storage storage, String path, List<?> defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            List<?> value = storage.getObjectList( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setObjectList( Storage storage, String path, List<?> value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setObjectList( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static String findString( Storage storage, String path, String defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            String value = storage.getString( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setString( Storage storage, String path, String value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setString( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static String[] findStringArray( Storage storage, String path, String[] defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            String[] value = storage.getStringArray( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setStringArray( Storage storage, String path, String[] value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setStringArray( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    public static Set<String> findStringSet( Storage storage, String path, Set<String> defaultValue ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            Set<String> value = storage.getStringSet( names[names.length - 1], defaultValue );
            unwalkPath( storage, names );
            return value;
        }
        return defaultValue;
    }

    public static void setStringSet( Storage storage, String path, Set<String> value ) {
        String[] names = walkPath( storage, path );
        if ( names != null ) {
            storage.setStringSet( names[names.length - 1], value );
            unwalkPath( storage, names );
        }
    }

    private static void unwalkPath( Storage storage, String[] names ) {
        for ( int i = 0; i < names.length - 1; i++ )
            storage.closeBox();
    }

    private static String[] walkPath( Storage storage, String path ) {
        if ( path != null ) {
            String[] names = path.split( "\\" + Storage.SEPARATOR );
            if ( names.length > 0 ) {
                for ( int i = 0; i < names.length - 1; i++ ) {
                    storage.openBox( names[i] );
                }

                return names;
            }
        }
        return null;
    }
}
