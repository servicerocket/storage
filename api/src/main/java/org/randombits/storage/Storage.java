/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.storage;

import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author David Peterson
 */
public interface Storage {

    /**
     * The character which separates box/property names in paths.
     */
    public static final char SEPARATOR = '.';

    /**
     * Retrieves the stored boolean value.
     * 
     * @param name
     *            The name the value was stored with.
     * @param def
     *            The value to return if no value is stored.
     * @return The stored value, or the default if nothing is stored with that
     *         name.
     * @throws StorageException
     *             if the named value is not a boolean.
     */
    public boolean getBoolean( String name, boolean def );

    /**
     * Retrieves the stored Boolean value.
     * 
     * @param name
     *            The name the value was stored with.
     * @param def
     *            The value to return if no value is stored.
     * @return The stored value, or the default if nothing is stored with that
     *         name.
     * @throws StorageException
     *             if the named value is not a Boolean.
     */
    public Boolean getBoolean( String name, Boolean def );

    /**
     * Returns the specified value as a Date.
     * 
     * @param name
     *            The name of the field to retrieve.
     * @param def
     *            The value to return if the date is not found.
     * @return The date value.
     * @throws StorageException
     *             if there was a problem converting the date.
     */
    public Date getDate( String name, Date def );

    /**
     * Returns the specified value as a JODA DateTime.
     * 
     * @param name
     *            The name of the field to retrieve.
     * @param def
     *            The value to return if the date is not found.
     * @return The date value.
     * @throws StorageException
     *             if there was a problem converting the date.
     */
    public DateTime getDateTime( String name, DateTime def );

    /**
     * Retrieves the stored double value.
     * 
     * @param name
     *            The name the value was stored with.
     * @param def
     *            The value to return if no value is stored.
     * @return The stored value, or the default if nothing is stored with that
     *         name.
     * @throws StorageException
     *             if the named value is not a double.
     */
    public double getDouble( String name, double def );

    /**
     * Retrieves the stored Double value.
     * 
     * @param name
     *            The name the value was stored with.
     * @param def
     *            The value to return if no value is stored.
     * @return The stored value, or the default if nothing is stored with that
     *         name.
     * @throws StorageException
     *             if the named value is not a Double.
     */
    public Double getDouble( String name, Double def );

    /**
     * Retrieves the stored integer value.
     * 
     * @param name
     *            The name the value was stored with.
     * @param def
     *            The value to return if no value is stored.
     * @return The stored value, or the default if nothing is stored with that
     *         name.
     * @throws StorageException
     *             if the named value is not an integer.
     */
    public int getInteger( String name, int def );

    /**
     * Retrieves the stored Integer value.
     * 
     * @param name
     *            The name the value was stored with.
     * @param def
     *            The value to return if no value is stored.
     * @return The stored value, or the default if nothing is stored with that
     *         name.
     * @throws StorageException
     *             if the named value is not an Integer.
     */
    public Integer getInteger( String name, Integer def );

    /**
     * Retrieves the stored long value.
     * 
     * @param name
     *            The name the value was stored with.
     * @param def
     *            The value to return if no value is stored.
     * @return The stored value, or the default if nothing is stored with that
     *         name.
     * @throws StorageException
     *             if the named value is not a long.
     */
    public long getLong( String name, long def );

    /**
     * Retrieves the stored Long value.
     * 
     * @param name
     *            The name the value was stored with.
     * @param def
     *            The value to return if no value is stored.
     * @return The stored value, or the default if nothing is stored with that
     *         name.
     * @throws StorageException
     *             if the named value is not a Long.
     */
    public Long getLong( String name, Long def );

    /**
     * Returns the named value as a {@link Number}. What the actual type of
     * Number returned is depends on both the stored value, type of
     * {@link Storage} and the default value. Assume nothing.
     * 
     * @param name
     *            The name of the value to return.
     * @param def
     *            The value to return if no value is stored.
     * @return The stored value, or the default if nothing is stord with that.
     */
    public Number getNumber( String name, Number def );

    /**
     * Retrieves the stored object value. Note that some object storage
     * mechanisms may not be very efficient. Always use the specific 'get' or
     * 'set' method for the type of value being stored, if it is available (e.g.
     * {@link #getInteger(String, int)} or {@link #getString(String, String)}).
     * 
     * @param name
     *            The name of the object to retrieve.
     * @param def
     *            The default value if the object has not been stored.
     * @return The object value, or the default if it was not found.
     */
    public Object getObject( String name, Object def );

    /**
     * Retrieves the stored object value, ensuring that it is an instance of the
     * specified class. If it is not an instance of <code>clazz</code>,
     * <code>null</code> is returned.
     * 
     * <p>
     * <b>Note:</b> The default value is not checked against <code>clazz</code>.
     * 
     * @param name
     *            The name of the object to retrieve.
     * @param def
     *            The default value if the object has not been stored or is not
     *            of the specified class.
     * @param clazz
     *            The class the object must be an instance of.
     * @return The object value, or default if it was not found, or was not an
     *         instance of <code>clazz</code>.
     */
    public <T> T getObject( String name, T def, Class<T> clazz );

    /**
     * Retrieves the list of objects.
     * 
     * @param name
     *            The name the objects were stored with.
     * @param def
     *            The value to return if no value is stored.
     * @return The stored value, or the default if nothing is stored with that
     *         name.
     * @throws StorageException
     *             if the named value is not a list of objects.
     */
    public List<?> getObjectList( String name, List<?> def );

    /**
     * Retrieves the stored string value.
     * 
     * @param name
     *            The name the value was stored with.
     * @param def
     *            The value to return if no value is stored.
     * @return The stored value, or the default if nothing is stored with that
     *         name.
     * @throws StorageException
     *             if the named value is not a String.
     */
    public String getString( String name, String def );

    /**
     * Retrieves the stored string array value.
     * 
     * @param name
     *            The name the value was stored with.
     * @param def
     *            The value to return if no value is stored.
     * @return The stored value, or the default if nothing is stored with that
     *         name.
     * @throws StorageException
     *             if the named value is not an array of Strings.
     */
    public String[] getStringArray( String name, String[] def );

    /**
     * Returns the string array in the specified scope as a Set of strings.
     * 
     * @param name
     *            The name of the field.
     * @param def
     *            The value to return if the field is not set.
     * @return the Set of Strings.
     * @see #getStringArray(String, String[])
     */
    public Set<String> getStringSet( String name, Set<String> def );

    /**
     * Check if the box is read-only.
     * 
     * @return <code>true</code> if the box cannot have any values saved.
     */
    public boolean isReadOnly();

    /**
     * Returns the set of non-absolute named values in the current context. May
     * return <code>null</code> if that cannot be determined.
     * 
     * @return The set of names.
     */
    public Set<String> nameSet();

    /**
     * Stores the value with the specified name.
     * 
     * @param name
     *            The name to store with.
     * @param value
     *            The value to store.
     */
    public void setBoolean( String name, boolean value );

    /**
     * Stores the value with the specified name.
     * 
     * @param name
     *            The name to store with.
     * @param value
     *            The value to store.
     */
    public void setBoolean( String name, Boolean value );

    /**
     * Stores the date value with the specified name.
     * 
     * @param name
     *            The name to store with.
     * @param value
     *            The value to store.
     * @throws UnsupportedOperationException
     *             if this storage is read-only
     * @see #isReadOnly()
     */
    public void setDate( String name, Date value );

    /**
     * Stores the date/time value with the specified name.
     * 
     * @param name
     *            The name to store with.
     * @param value
     *            The value to store.
     * @throws UnsupportedOperationException
     *             if this storage is read-only
     * @see #isReadOnly()
     */
    public void setDateTime( String name, DateTime value );

    /**
     * Stores the value with the specified name.
     * 
     * @param name
     *            The name to store with.
     * @param value
     *            The value to store.
     */
    public void setDouble( String name, double value );

    /**
     * Stores the value with the specified name.
     * 
     * @param name
     *            The name to store with.
     * @param value
     *            The value to store.
     */
    public void setDouble( String name, Double value );

    /**
     * Stores the value with the specified name.
     * 
     * @param name
     *            The name to store with.
     * @param value
     *            The value to store.
     */
    public void setInteger( String name, int value );

    /**
     * Stores the value with the specified name.
     * 
     * @param name
     *            The name to store with.
     * @param value
     *            The value to store.
     */
    public void setInteger( String name, Integer value );

    /**
     * Stores the value with the specified name.
     * 
     * @param name
     *            The name to store with.
     * @param value
     *            The value to store.
     */
    public void setLong( String name, long value );

    /**
     * Stores the value with the specified name.
     * 
     * @param name
     *            The name to store with.
     * @param value
     *            The value to store.
     */
    public void setLong( String name, Long value );

    /**
     * Stores the object value.
     * 
     * @param name
     *            The name of the object value.
     * @param value
     *            The value to store.
     */
    public void setObject( String name, Object value );

    /**
     * Stores the list of objects.
     * 
     * @param name
     *            The name of the object list.
     * @param value
     *            The object list to store.
     */
    public void setObjectList( String name, List<? extends Object> value );

    /**
     * Stores the value with the specified name.
     * 
     * @param name
     *            The name to store with.
     * @param value
     *            The value to store.
     * @throws UnsupportedOperationException
     *             if this storage is read-only.
     * @see #isReadOnly()
     */
    public void setString( String name, String value );

    /**
     * Stores the value with the specified name.
     * 
     * @param name
     *            The name to store with.
     * @param value
     *            The value to store.
     * @throws UnsupportedOperationException
     *             if this storage is read-only.
     * @see #isReadOnly()
     */
    public void setStringArray( String name, String[] value );

    /**
     * Saves the Set of strings to as a String array.
     * 
     * @param name
     *            The field name.
     * @param values
     *            The Set of strings to save.
     */
    public void setStringSet( String name, Set<String> values );

    /**
     * Opens a box with the specified name contained within the parent box. The
     * new box will be where any values get stored until the box is closed
     * again. If the box hasn't been opened before, a new one is created.
     * 
     * @param name
     *            The name of the box.
     * @throws DuplicateNameException
     *             if a different value with the same name already exists.
     */
    public void openBox( String name );

    /**
     * Makes an absolute path for the current state, with no trailing
     * {@link #SEPARATOR}.
     * 
     * @return The absolute path.
     */
    public String makePath();

    /**
     * Makes an absolute path for the current state without having separator.
     *
     * @return The absolute path in array of string.
     */
    public List<String> makePathAsArray();

    /**
     * Makes an absolute path for the value without having separator with the specified name.
     *
     * @param child
     *            The name of the child value.
     * @return the absolute path in array of string.
     */
    public List<String> makePathAsArray(String child);

    /**
     * Makes an absolute path for the value with the specified name.
     * 
     * @param child
     *            The name of the child value.
     * @return the absolute path.
     */
    public String makePath( String child );

    /**
     * Closes the current box, returning to the parent box.
     */
    public void closeBox();

    /**
     * Attempts to remove the specified child box. If the storage implementation
     * does not support box removal, it should return <code>false</code>. If
     * there was an error in the process of removing the box, a
     * {@link StorageException} should be thrown. If it was removed
     * successfully, <code>true</code> should be returned.
     * 
     * @param name
     *            The name of the box to remove.
     * @return <code>true</code> if the box was removed.
     * @throws StorageException
     *             if there was an infrastructure problem.
     */
    public boolean removeBox( String name );

    /**
     * Adds the listener.
     * 
     * @param listener
     *            The listener.
     */
    void addStorageListener( StorageListener listener );

    /**
     * Removes the listener.
     * 
     * @param listener
     *            The listener.
     */
    void removeStorageListener( StorageListener listener );
}