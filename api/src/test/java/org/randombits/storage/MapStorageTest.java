package org.randombits.storage;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author longyc, willian
 * @since 5.3.0.20150406.
 */
@RunWith(MockitoJUnitRunner.class)
public class MapStorageTest {

    MapStorage $;

    @Before public void setUp() throws Exception {
        Map baseMap = new HashMap<String, Object>();

        $ = new MapStorage(baseMap);
    }

    @Test public void shouldReturnPathString() {
        $.openBox("repeatingdata001");
        String pathName = $.makePath();
        String expectedPathName = "repeatingdata001";

        assertThat("Should return path of current stack in string.", pathName.equals(expectedPathName));

        $.openBox("0");
        String appendedPathName = $.makePath("textdata001");
        String expectedAppendedPathName = "repeatingdata001.0.textdata001";

        assertThat("Should return path of current stack with appended name in string.", pathName.equals(expectedPathName));
    }

    @Test public void shouldReturnPathArray() {
        $.openBox("repeatingdata002");
        List<String> pathArray = $.makePathAsArray();
        List<String> expectedPathArray = new ArrayList<>();
        expectedPathArray.add(0, "repeatingdata002");

        assertThat("Should return path of current stack in array.", pathArray, equalTo(expectedPathArray));

        $.openBox("0");
        List<String> appendedpathArray = $.makePathAsArray("textdata002");
        expectedPathArray.add(1, "0");
        expectedPathArray.add(2, "textdata002");

        assertThat("Should return path of current stack with appended name in array.", appendedpathArray, equalTo(expectedPathArray));
    }
}