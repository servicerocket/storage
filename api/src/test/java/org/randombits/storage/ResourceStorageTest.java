package org.randombits.storage;

import junit.framework.TestCase;

import java.util.Locale;

public class ResourceStorageTest extends TestCase {

    Locale originalLocale = Locale.getDefault();

    ResourceStorage enStorage;

    ResourceStorage deStorage;

    ResourceStorage frStorage;

    @Override protected void setUp() throws Exception {
        super.setUp();

        // Set English as the default for a baseline.
        Locale.setDefault( Locale.ENGLISH );
        enStorage = new ResourceStorage( ResourceStorageTest.class );

        // A German alternative exists.
        deStorage = new ResourceStorage( ResourceStorageTest.class, Locale.GERMAN );
        // No French alternative exists.
        frStorage = new ResourceStorage( ResourceStorageTest.class, Locale.FRENCH );
    }

    @Override protected void tearDown() throws Exception {
        super.tearDown();
        enStorage = null;
        deStorage = null;
        frStorage = null;

        Locale.setDefault( originalLocale );
    }

    public void testGetMessageStringStringObjectArray() {
        String message;

        message = enStorage.getMessage( "greeting", "Hi {0}", "John" );
        assertEquals( "Good morning, John.", message );

        // German is customised.
        message = deStorage.getMessage( "greeting", "Hullo {0}", "Johan" );
        assertEquals( "Guten Morgen, Johan.", message );

        // No French is available.
        message = frStorage.getMessage( "greeting", "Hi {0}", "Jaque" );
        assertEquals( "Good morning, Jaque.", message );

        message = enStorage.getMessage( "english.only", "{0} only", "English" );
        assertEquals( "Only English.", message );

        message = deStorage.getMessage( "english.only", "Nur {0}", "Deutsch" );
        assertEquals( "Only English.", message );

        message = enStorage.getMessage( "german.only", "German Only", "Deutsch" );
        assertEquals( "German Only", message );

        message = deStorage.getMessage( "german.only", "German Only", "Deutsch" );
        assertEquals( "Nur Deutsch.", message );

    }

    public void testGetMessageString() {
        String message;

        message = enStorage.getMessage( "greeting" );
        assertEquals( "Good morning, {0}.", message );

        // German is customised.
        message = deStorage.getMessage( "greeting" );
        assertEquals( "Guten Morgen, {0}.", message );

        // No French is available.
        message = frStorage.getMessage( "greeting" );
        assertEquals( "Good morning, {0}.", message );
    }

}
