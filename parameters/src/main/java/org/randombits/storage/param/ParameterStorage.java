package org.randombits.storage.param;

import org.randombits.storage.IndexedStorage;

public interface ParameterStorage<V> extends IndexedStorage {
    void add( Parameter<V> param );
}
