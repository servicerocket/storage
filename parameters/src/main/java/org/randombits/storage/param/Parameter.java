package org.randombits.storage.param;

public class Parameter<V> {
    private String key;

    private V value;

    public Parameter( String key, V value ) {
        this.key = key;
        this.value = value;
    }

    String getKey() {
        return key;
    }

    V getValue() {
        return value;
    }
    
    void setValue( V value ) {
        this.value = value;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( obj instanceof Parameter ) {
            Parameter<?> param = ( Parameter<?> ) obj;
            return isEqual( key, param.getKey() ) && isEqual( value, param.getValue() );
        }
        return false;
    }

    private boolean isEqual( Object o1, Object o2 ) {
        if ( o1 == null )
            return o2 == null;
        return o1.equals( o2 );
    }

    @Override
    public int hashCode() {
        int hash = key.hashCode();
        if ( value != null )
            hash += value.hashCode();
        hash += 24;
        return hash;
    }
    
    protected void setKey( String key ) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "{ " + key + ": " + value + " }";
    }
}
