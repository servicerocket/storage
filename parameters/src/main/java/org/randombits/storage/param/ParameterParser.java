/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.storage.param;

import java.util.regex.Pattern;

/**
 * This class helps parse parameter strings and lists of parameter strings.
 * 
 * @author David Peterson
 */
public class ParameterParser {
    private final char escape;

    private final char paramSeparator;

    private final char assignmentSeparator;

    private final Pattern paramSplitter;

    private final Pattern assignmentSplitter;

    private final Pattern escapePattern;

    private final boolean unescapedAssignmentAllowed;

    public ParameterParser( char assignmentSeparator, char paramSeparator, char escape ) {
        this( assignmentSeparator, paramSeparator, escape, true );
    }

    public ParameterParser( char assignmentSeparator, char paramSeparator, char escape,
            boolean unescapedAssignmentAllowed ) {
        this.escape = escape;
        this.paramSeparator = paramSeparator;
        this.assignmentSeparator = assignmentSeparator;
        this.unescapedAssignmentAllowed = unescapedAssignmentAllowed;

        paramSplitter = compileEscapedSplitter( escape, paramSeparator );
        assignmentSplitter = compileEscapedSplitter( escape, assignmentSeparator );
        escapePattern = Pattern.compile( "\\" + escape + "(.)" );
    }

    /**
     * Compiles a {@link Pattern} which will split any strings at occurrences of
     * an unescaped separator sequence.
     * 
     * @param escape
     *            The regular expression for the escape prefix.
     * @param separator
     *            The regular expression for the separator sequence.
     * @return
     */
    public static Pattern compileEscapedSplitter( char escape, char separator ) {
        StringBuilder pattern = new StringBuilder();
        pattern.append( "(?<!(^|[^\\" ).append( escape ).append( "])" ).append( "(\\" ).append( escape ).append(
                '\\' ).append( escape ).append( "){0,100}\\" ).append( escape ).append( ")\\" ).append( separator );
        return Pattern.compile( pattern.toString() );
    }

    public ParameterStorage<String> parseList( String parameterList ) throws ParameterParsingException {
        return parseList( parameterList, false );
    }

    /**
     * Parses the specified parameter list string. The parameter list string is
     * expected to <i>only</i> contain a valid parameter list.
     * 
     * @param parameterList
     *            The parameter list string.
     * @param requireKey
     *            If <code>true</code>, unnamed parameters will be allowed at
     *            the start of the list.
     * @return The parameter storage instance.
     * @throws ParameterParsingException
     *             if there is a problem parsing the parameter list.
     */
    public ParameterStorage<String> parseList( String parameterList, boolean requireKey )
            throws ParameterParsingException {
        StringParameterStorage storage = new StringParameterStorage();

        if ( parameterList != null && parameterList.length() > 0 ) {
            String[] paramsArray = paramSplitter.split( parameterList );
            int index = requireKey ? -1 : 0;
            for ( String paramValue : paramsArray ) {
                Parameter<String> param = parseParameter( paramValue, index < 0 );
                // Check if the parameter is indexed.
                if ( param.getKey() == null ) {
                    param.setKey( String.valueOf( index ) );
                    index++;
                } else {
                    index = -1;
                }
                storage.add( param );
            }
        }
        return storage;
    }

    /**
     * Parses a single parameter. Eg. If the 'assignment' pattern is '=',
     * "name=value" will have the key "name" and value of "value".
     * 
     * @param parameter
     *            The parameter string.
     * @return The parameter.
     * @throws ParameterParsingException
     *             if there was a problem while parsing.
     */
    public Parameter<String> parseParameter( String parameter ) throws ParameterParsingException {
        return parseParameter( parameter, true );
    }

    public Parameter<String> parseParameter( String parameter, boolean requireKey )
            throws ParameterParsingException {
        String key, value;

        String[] paramArray = assignmentSplitter.split( parameter, unescapedAssignmentAllowed ? 2 : -1 );
        if ( paramArray.length == 1 ) {
            key = null;
            value = paramArray[0];
        } else if ( paramArray.length == 2 ) {
            key = paramArray[0].trim();
            value = paramArray[1];
        } else {
            throw new ParameterParsingException( "Invalid parameter: " + parameter );
        }

        key = unescape( key );
        value = unescape( value );

        if ( ( key == null || key.length() == 0 ) ) {
            if ( requireKey )
                throw new ParameterParsingException( "Unexpected unnamed parameter: " + parameter );
            else
                key = null;
        }

        return new Parameter<String>( key, value );
    }

    private String unescape( String string ) {
        if ( string != null )
            return escapePattern.matcher( string ).replaceAll( "$1" );
        return null;
    }

    public char getEscape() {
        return escape;
    }

    public char getParamSeparator() {
        return paramSeparator;
    }

    public char getAssignmentSeparator() {
        return assignmentSeparator;
    }

    public boolean isUnescapedAssignmentAllowed() {
        return unescapedAssignmentAllowed;
    }
}
