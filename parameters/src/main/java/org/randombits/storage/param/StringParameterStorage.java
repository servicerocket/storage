package org.randombits.storage.param;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.randombits.storage.StringBasedStorage;

public class StringParameterStorage extends StringBasedStorage implements ParameterStorage<String> {

    protected Map<String, Parameter<String>> params;

    // Base set of names.
    protected Set<String> names;

    // Unmodifiable set of names.
    private Set<String> uNames;

    public StringParameterStorage() {
        super( BoxType.Virtual );
        params = new java.util.HashMap<String, Parameter<String>>();
        names = new java.util.HashSet<String>();
        uNames = Collections.unmodifiableSet( names );
    }

    public void addAll( Collection<Parameter<String>> params ) {
        for ( Parameter<String> param : params ) {
            add( param );
        }
    }

    public void add( Parameter<String> param ) {
        checkReadOnly();
        params.put( param.getKey().toLowerCase(), param );
        names.add( param.getKey() );
    }

    @Override
    protected Set<String> baseNameSet() {
        return uNames;
    }

    @Override
    protected String getBaseString( String name ) {
        Parameter<String> param = params.get( name.toLowerCase() );
        return param == null ? null : param.getValue();
    }

    @Override
    protected void setBaseString( String name, String value ) {
        name = name.toLowerCase();
        Parameter<String> param = params.get( name );
        if ( param == null ) {
            add( new Parameter<String>( name, value ) );
        } else {
            param.setValue( value );
        }
    }

    @Override
    protected String[] getBaseStringArray( String name ) {
        String value = getBaseString( name );
        if ( value != null ) {
            return value.split( "(?<!\\\\),\\s*" );
        }
        return null;
    }

    @Override
    protected void setBaseStringArray( String name, String[] value ) {
        String stringValue = null;
        if ( value != null ) {
            StringBuilder out = new StringBuilder();
            for ( int i = 0; i < value.length; i++ ) {
                if ( i != 0 )
                    out.append( ", " );
                out.append( value[i].replaceAll( ",", "\\," ) );
            }
        }
        setBaseString( name, stringValue );
    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    public boolean getBoolean( int index, boolean def ) {
        return getBoolean( String.valueOf( index ), def );
    }

    public Boolean getBoolean( int index, Boolean def ) {
        return getBoolean( String.valueOf( index ), def );
    }

    public Date getDate( int index, Date def ) {
        return getDate( String.valueOf( index ), def );
    }

    public DateTime getDateTime( int index, DateTime def ) {
        return getDateTime( String.valueOf( index ), def );
    }

    public double getDouble( int index, double def ) {
        return getDouble( String.valueOf( index ), def );
    }

    public Double getDouble( int index, Double def ) {
        return getDouble( String.valueOf( index ), def );
    }

    public int getInteger( int index, int def ) {
        return getInteger( String.valueOf( index ), def );
    }

    public Integer getInteger( int index, Integer def ) {
        return getInteger( String.valueOf( index ), def );
    }

    public long getLong( int index, long def ) {
        return getLong( String.valueOf( index ), def );
    }

    public Long getLong( int index, Long def ) {
        return getLong( String.valueOf( index ), def );
    }

    public Object getObject( int index, Object def ) {
        return getObject( String.valueOf( index ), def );
    }

    public List<?> getObjectList( int index, List<?> def ) {
        return getObjectList( String.valueOf( index ), def );
    }

    public String getString( int index, String def ) {
        return getString( String.valueOf( index ), def );
    }

    public String[] getStringArray( int index, String[] def ) {
        return getStringArray( String.valueOf( index ), def );
    }

    public Set<String> getStringSet( int index, Set<String> def ) {
        return getStringSet( String.valueOf( index ), def );
    }
}
